package buu.satja.math

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import buu.satja.math.databinding.FragmentMainBinding

class MainFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentMainBinding>(inflater,
            R.layout.fragment_main,container,false)
        binding.menu = this

        binding.apply {
            btnAdd.setOnClickListener { view ->
                view.findNavController().navigate(R.id.action_mainFragment_to_addFragment)
            }
            btnMinus.setOnClickListener { view ->
                view.findNavController().navigate(R.id.action_mainFragment_to_minusFragment)
            }
            btnMultiply.setOnClickListener { view ->
                view.findNavController().navigate(R.id.action_mainFragment_to_multiplyFragment)
            }
        }
        return binding.root
    }

}