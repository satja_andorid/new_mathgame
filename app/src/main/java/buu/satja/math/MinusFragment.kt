package buu.satja.math

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.satja.math.databinding.FragmentMinusBinding

class MinusFragment : Fragment() {
    var correct: Int = 0
    var incorrect: Int = 0
    private lateinit var binding: FragmentMinusBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentMinusBinding>(inflater,
            R.layout.fragment_minus,container,false)
        binding.gameminus = this


        fun play() {
            val btnBack = binding.btnBack
            val btnReset = binding.btnReset

            val Num1 = binding.num1
            val Num2 = binding.num2
            val btnAns1 = binding.btnAns1
            val btnAns2 = binding.btnAns2
            val btnAns3 = binding.btnAns3
            val txtShow = binding.txtShowAns
            val txtStatic = binding.txttrue
            val btnPlay = binding.btnAgain
            btnAns1.setBackgroundColor(Color.GRAY)
            btnAns2.setBackgroundColor(Color.GRAY)
            btnAns3.setBackgroundColor(Color.GRAY)
            var isSelectAnswer = false
            val ran1 = (0..9).random()
            val ran2 = (0..9).random()

            val result = ran1 - ran2

            Num1.setText(ran1.toString())
            Num2.setText(ran2.toString())
            val result1 = result+(1..5).random()
            var result2 = result+(1..5).random()
            while (true){
                if (result1 == result2){
                    result2 = result+(1..5).random()
                }else{
                    break;
                }
            }

            val c = (1..3).random()
            if (c==1){
                btnAns1.text = result.toString()
                btnAns2.text = result1.toString()
                btnAns3.text = result2.toString()

            }
            if (c==2){
                btnAns2.text = result.toString()
                btnAns3.text = result1.toString()
                btnAns1.text = result2.toString()
            }
            if (c==3){
                btnAns3.text = result.toString()
                btnAns1.text = result1.toString()
                btnAns2.text = result2.toString()
            }
            btnAns1.setOnClickListener {
                if (c == 1) {
                    if (!isSelectAnswer){
                        btnAns1.setBackgroundColor(Color.GREEN)
                        txtShow.text = "CORRECT"
                        correct++
                        txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                        isSelectAnswer = true
                    }
                } else {
                    if (!isSelectAnswer){
                        btnAns1.setBackgroundColor(Color.RED)
                        txtShow.text = "INCORRECT"
                        incorrect++
                        txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                        isSelectAnswer = true
                    }
                }
            }
            btnAns2.setOnClickListener {
                if (c == 2) {
                    if (!isSelectAnswer){
                        btnAns2.setBackgroundColor(Color.GREEN)
                        txtShow.text = "CORRECT"
                        correct++
                        txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                        isSelectAnswer = true
                    }
                } else {
                    if (!isSelectAnswer){
                        btnAns2.setBackgroundColor(Color.RED)
                        txtShow.text = "INCORRECT"
                        incorrect++
                        txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                        isSelectAnswer = true
                    }
                }
            }
            btnAns3.setOnClickListener {
                if (c == 3) {
                    if (!isSelectAnswer){
                        btnAns3.setBackgroundColor(Color.GREEN)
                        txtShow.text = "CORRECT"
                        correct++
                        txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                        isSelectAnswer = true
                    }
                } else {
                    if (!isSelectAnswer){
                        btnAns3.setBackgroundColor(Color.RED)
                        txtShow.text = "INCORRECT"
                        incorrect++
                        txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                        isSelectAnswer = true
                    }
                }
            }
            btnPlay.setOnClickListener {
                play()
            }
            btnBack.setOnClickListener { view ->
                view.findNavController()
                    .navigate(R.id.action_minusFragment_to_mainFragment)
            }
            btnReset.setOnClickListener {
                correct = 0;
                incorrect = 0;
                play()
                txtStatic.text = "correct:${correct} incorrect:${incorrect}"
            }


        }
        play()
        return binding.root
    }
}